@extends('layouts.appt')

@section('content')
    <div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Listar Visitas</h5>
                        <h6 class="card-subtitle text-muted">visitas realizadas por los pastores y misioneros</h6>
                        <br>
                        {{-- <input type="text" id="fecharango"> --}}
                        <div class="row">
                            <div class="col-12 col-xl-4">
                                <div class="mb-3 mb-xl-0">
                                    <label class="form-label">Distrito</label>
                                    <select id="inputState" class="form-select">
                                        <option selected="">Uctubamba</option>
                                        <option>Chiclayo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-xl-8">
                                <div class="mb-3 mb-xl-0">
                                    <label class="form-label">Fecha de visitas</label>
                                    <div id="reportrange" class="overflow-hidden form-control">
                                        <i class="far fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fas fa-caret-down"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatables-reponsive" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%">Feligres</th>
                                    <th style="width:20%">Distrito</th>
                                    <th style="width:20%">Tipo visita</th>
                                    <th style="width:5%">Asistentes</th>
                                    <th style="width:20%">Fecha visita</th>
                                    {{-- <th class="d-none d-md-table-cell" style="width:25%">Date of Birth</th> --}}
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($visitas as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item->apellidos }}, {{ $item->nombres }}</td>
                                        <td>{{ $item->distrito }}
                                            <div class="small">{{ $item->pastor }}</div>
                                        </td>
                                        <td>{{ $item->tipo }}</td>
                                        <td>{{ $item->participantes }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        {{-- <td class="d-none d-md-table-cell">June 21, 1961</td> --}}
                                        <td class="table-action">
                                            <a href="#"><i class="align-middle" data-feather="edit-2"></i></a>
                                            <a href="#"><i class="align-middle" data-feather="trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Datatables Responsive
            $("#datatables-reponsive").DataTable({
                responsive: true
            });
            // Daterangepicker
            $("input[name=\"daterange\"]").daterangepicker({
                opens: "left"
            });
            $("input[name=\"datetimes\"]").daterangepicker({
                timePicker: true,
                opens: "left",
                startDate: moment().startOf("hour"),
                endDate: moment().startOf("hour").add(32, "hour"),
                locale: {
                    format: "M/DD hh:mm A"
                }
            });
            $("input[name=\"datesingle\"]").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            var start = moment().subtract(29, "days");
            var end = moment();

            function cb(start, end) {
                console.log(start.format("YYYY-MM-D"), ' - ', end.format("YYYY-MM-D"))

                $("#fecharango").val(start.format("YYYY-MM-D") + '/' + end.format("YYYY-MM-D"));
                $("#reportrange span").html(start.format("MMMM D, YYYY") + " - " + end.format("MMMM D, YYYY"));
            }
            $("#reportrange").daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1,
                        "month").endOf("month")]
                }
            }, cb);
            cb(start, end);
        });
    </script>
@endsection
