@extends('layouts.appt')

@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                {{-- <li class="breadcrumb-item"><a href="{{route('dashboard')}} ">Home</a></li> --}}
                <li class="breadcrumb-item active">form</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')

<div>
    <div class="col-md-12">
        <div class="card card-default ">
            <div class="card-header">
            <h3 class="card-title">Actualizar Personal</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            </div>
            <form action="{{route('personal.update', $user->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                            <label>Nombres *</label>
                            <input type="text" class="form-control" name="nombres" value="{{$user->persona->nombres}}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Apellidos *</label>
                            <input type="text" class="form-control" name="apellidos" value="{{$user->persona->apellidos}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                            <label>Nro Doc</label>
                            <input type="text" class="form-control" name="nrodoc" value="{{$user->persona->nrodoc}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Celular *</label>
                            <input type="text" class="form-control" name="celular" value="{{$user->persona->celular}}" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Email</label>
                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->persona->email}}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Firma</label>
                            <input type="file" class="form-control" name="firma" value="">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div>
                                {{-- <img src="{{asset($user->persona->firma)}}" alt="" width="150px"> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Cargo</label>
                                <div class="form-group">
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="customRadio0" name="tipo" value="0" required {{ 0 == old('tipo', $user->persona->tipo) ? 'checked' : '' }}>
                                        <label for="customRadio0" class="custom-control-label">Usario</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="customRadio1" name="tipo" value="1" required {{ 1 == old('tipo', $user->persona->tipo) ? 'checked' : '' }}>
                                        <label for="customRadio1" class="custom-control-label">Pastor Distrital - (O)</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="customRadio2" name="tipo" value="2" required {{ 2 == old('tipo', $user->persona->tipo) ? 'checked' : '' }}>
                                        <label for="customRadio2" class="custom-control-label">Administrativo - (O)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Estado</label>
                                <div class="form-group">
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="customRadio3" name="estado" value="1" required {{ 1 == old('estado', $user->persona->estado) ? 'checked' : '' }}>
                                        <label for="customRadio3" class="custom-control-label">Activo</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="customRadio4" name="estado" value="0" required {{ 0 == old('estado', $user->persona->estado) ? 'checked' : '' }}>
                                        <label for="customRadio4" class="custom-control-label">Inactivo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Archivo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="form-control" name="archivo" required>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-12">
                            <div class="card card-primary card-outline card-outline-tabs">
                                <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-bs-toggle="tab" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Acceso al sistema</a>
                                    </li>
                                    <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-three-messages-tab" data-bs-toggle="tab" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Asignar Distrito</a>
                                    </li>
                                </ul>
                                </div>
                                <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                        <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-6">
                                        <div class="form-group row">
                                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>
                                            <div class="col-md-6">
                                                <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $user->username }}" required autocomplete="username"> 
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>                    
                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>                    
                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-6">
                                            <label for="">Asignar Roles</label>
                                            <ul>
                                                {{-- @foreach ($roles as $role)
                                                <li>
                                                    <label>
                                                        <input type="checkbox" name="roles[]" value="{{ $role->id }}"> {{ $role->name }}
                                                        <em>({{ $role->guard_name }})</em>
                                                    </label>
                                                </li>    
                                                @endforeach --}}

                                                @foreach($roles as $role)
                                                <li>
                                                    <label>
                                                        <input type="checkbox" name="roles[]" 
                                                        @foreach ( $userRole as $rolexist )
                                                                {{ $rolexist->id == $role->id ? 'checked' : '' }}
                                                        @endforeach
                                                            value="{{ $role->id }}"> {{ $role->name }}
                                                        <em>({{ $role->guard_name }})</em>	        
                                                    </label>
                                                </li>
                                                @endforeach
                                                
                                            </ul>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
                                        <div class="form-group row">
                                            <label >Distrito</label>                    
                                            <div class="col-md-6">
                                                <select name="distrito_id" id="" class="form-control">
                                                    <option value="0">-Seleccione-</option>
                                                    @foreach ($distritos as $distrito)

                                                    @if ($user->persona->distrito)
                                                        <option value="{{$distrito->id}}"
                                                                @if($distrito->id == $user->persona->distrito->id) 
                                                                    selected
                                                                @endif
                                                            >{{$distrito->distrito}} </option>
                                                    @else
                                                        <option value="{{$distrito->id}}">{{$distrito->distrito}}</option>
                                                    @endif                                                            
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                    <a href="{{route('personal.index')}}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Actualizar Usuario" class="btn btn-success float-right">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>  
</div>

@endsection