@extends('layouts.appt')

@section('content')
    <div>
        {{-- <h1 class="h3 mb-3">Listar</h1> --}}
        <div class="col-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Usuarios</h5>
                    <h6 class="card-subtitle text-muted">Lista de Usuarios</h6>
                    <a href="{{route('personal.create')}} " class="btn btn-success float-right">Nuevo Usuario</a>
                </div>
                <div class="card-body">
                    <table id="datatables-persona" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tipo</th>
                                <th>Nombres y Apellidos</th>
                                <th>Username</th>
                                <th>Rol</th>
                                <th>Celular</th>
                                <th>Distrito</th>
                                <th>Estado</th>
                                <th>Opc</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $index => $user)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    @if ($user->persona->tipo == 1)
                                        <td><small class="badge bg-info">Pastor Distrital</small></td>    
                                    @elseif($user->persona->tipo == 2)
                                    <td><small class="badge bg-warning">Administrativo</small></td>    
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{$user->persona->nombres}} {{$user->persona->apellidos}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>
                                        @foreach ($user->getRoleNames() as $item)
                                        {{$item}}, 
                                        @endforeach
                                    </td>
                                    <td>{{$user->persona->celular}}</td>
                                    @if ($user->persona->distrito)
                                        <td>{{$user->persona->distrito->distrito}}</td>
                                    @else    
                                        <td></td>
                                    @endif
                                    
                                    @if ($user->persona->estado == 1)
                                        <td><small class="badge bg-success">Activo</small></td>
                                    @else
                                        <td><small class="badge bg-danger">Inactivo</small></td>
                                    @endif
                                    
                                    <td>
                                        <a href="{{ url('admin/personal/'.$user->id) }}" class="btn btn-block btn-outline-info btn-sm">Editar</a>
                                    </td>
                                    {{-- {{url('editar_informe_mes?id='.$mip->id.'')}} --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            $("#datatables-persona").DataTable({
                responsive: true
            });
        });
    </script>
@endsection
