<li class="sidebar-header">
    Modulos
</li>
<li class="sidebar-item active">
    <a data-bs-target="#admin" data-bs-toggle="collapse" class="sidebar-link">
        <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Admin</span>
    </a>
    <ul id="admin" class="sidebar-dropdown list-unstyled collapse show" data-bs-parent="#sidebar">
        <li class="sidebar-item active"><a class="sidebar-link" href="/home">Home</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="/listarvisitas">Visitas Realizadas</a></li>
    </ul>
</li>
<li class="sidebar-item active">
    <a data-bs-target="#indicadores" data-bs-toggle="collapse" class="sidebar-link">
        <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Indicadores</span>
    </a>
    <ul id="indicadores" class="sidebar-dropdown list-unstyled collapse show" data-bs-parent="#sidebar">
        <li class="sidebar-item"><a class="sidebar-link" href="/listardemo">Visitaciones</a></li>
    </ul>
</li>

<li class="sidebar-item active">
    <a data-bs-target="#mantenimiento" data-bs-toggle="collapse" class="sidebar-link">
        <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Mantenimiento</span>
    </a>
    <ul id="mantenimiento" class="sidebar-dropdown list-unstyled collapse show" data-bs-parent="#sidebar">
        <li class="sidebar-item"><a class="sidebar-link" href="{{url('admin/personal')}}">Usuarios</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Regiones</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Distritos</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Iglesias</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Pastores</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Feligreses</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Tipo Feligres</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Tipo visitas</a></li>
        <li class="sidebar-item"><a class="sidebar-link" href="#">Cargo</a></li>
    </ul>
</li>
