<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitacione extends Model
{
    use HasFactory;

    // public function persona()
    // {
    //     return $this->belongsTo(Persona::class, 'persona_id');
    // }

    public function tipovisita()
    {
        return $this->belongsTo(Tipovisita::class, 'tipovisita_id');
    }

    public function distrito()
    {
        return $this->belongsTo(Distrito::class, 'distrito_id');
    }
    
}
