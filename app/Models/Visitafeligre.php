<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitafeligre extends Model
{
    use HasFactory;

    public function feligres()
    {
        return $this->belongsTo(Feligre::class, 'feligre_id');
    }

    public function visitacion()
    {
        return $this->belongsTo(Visitacione::class, 'visitacione_id');
    }
}
