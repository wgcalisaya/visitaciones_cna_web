<?php

namespace App\Http\Controllers\Visitaciones;

use App\Http\Controllers\Controller;
use App\Models\Visitacione;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $visitas = DB::table('visitaciones')->get();
        // $visitas = Visitacione::with('feligres','tipovisita')
        // ->leftJoin('personas','personas.id','=','visitaciones.persona_id')
        // ->leftJoin('distritos','distritos.id','=','visitaciones.distrito_id')
        // ->select('visitaciones.*','personas.apellidos','distritos.distrito')
        // ->get();

        $visitas = DB::table('visitafeligres')
        ->leftJoin('feligres','feligres.id','=','visitafeligres.feligre_id')
        ->leftJoin('visitaciones','visitaciones.id','=','visitafeligres.visitacione_id')
        ->leftJoin('tipovisitas','tipovisitas.id','=','visitaciones.tipovisita_id')
        ->leftJoin('distritos','distritos.id','=','visitaciones.distrito_id')
        ->leftJoin('personas','personas.id','=','visitaciones.persona_id')
        ->select('feligres.nombres','feligres.apellidos','distritos.distrito','visitafeligres.created_at','tipovisitas.tipo','visitaciones.participantes','visitaciones.lugarvisita','personas.apellidos as pastor')
        ->get();
        // dd($visitas);
        return view('admin.visitas.index', compact('visitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
