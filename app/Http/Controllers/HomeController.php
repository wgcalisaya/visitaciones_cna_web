<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $user = User::where('email', 'wgcalisaya@gmail.com')->first();
        // $user->assignRole('lector');
        // $token = $user->createToken("auth_token")->plainTextToken;
        return view('home');
    }

    public function demo()
    {
        return view('admin/demo/index');
    }
}
