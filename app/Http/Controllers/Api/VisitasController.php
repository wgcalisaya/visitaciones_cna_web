<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Feligre;
use App\Models\Persona;
use App\Models\Visitacione;
use App\Models\Visitafeligre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class VisitasController extends Controller
{
    public function crearVisita(Request $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();

            $persona = Persona::where('user_id', auth()->user()->id)->first();
            $distrito = DB::table('distritos')->where('persona_id', $persona->id)->first();
            $distr_pers = DB::table('personadistritos')->where('persona_id', $persona->id)->where('estado', 1)->first();


            if ($distr_pers) {
                $idper_dist = $distr_pers->id;
            } else {
                $idper_dist = null;
            }

            if ($distrito) {
                $distritoid = $distrito->id;
            }else{
                $distritoid = null;
            }

            $request->validate([
                'tipovisita_id' => 'required',
                'nombres' => 'required',
                'cantidad' => 'required'
            ]);

            if ($request->visita_id) {
                $visita_head_id =  $request->visita_id;
            }else{
                
                $visita = new Visitacione();
                // $visita->feligre_id = $feligres->id;
                $visita->distrito_id = $distritoid;
                $visita->participantes = $request->cantidad;
                $visita->lugarvisita = $request->lugarvisita;
                $visita->tipovisita_id = $request->tipovisita_id;
                $visita->personadistrito_id = $idper_dist;
                $visita->persona_id = $persona->id;
                $visita->lugarvisita_id = $request->lugarvisita_id;
                if ($request->fechavisita) {
                    $visita->created_at = $request->fechavisita;
                }
                
                $visita->save();

                $visita_head_id =  $visita->id;
            }

            // if ($request->celular || $request->dni) {
            //     # code...
            // }
            
            
            if ($request->celular || $request->dni) {

                $feligres = DB::table('feligres')->where('celular', $request->celular)->orWhere('dni', $request->dni)->first();

                if (!$feligres) {
                    $feligres = new Feligre();
                    $feligres->dni = $request->dni;
                    $feligres->nombres = Str::title($request->nombres);
                    $feligres->apellidos = Str::title($request->apellidos);
                    $feligres->celular = $request->celular;
                    $feligres->estado = 1;
                    $feligres->save();
                }
                
            } else {
                // $request->validate([
                //     'nombres' => 'required',
                // ]);
                $feligres = new Feligre();
                $feligres->dni = $request->dni;
                $feligres->nombres = Str::title($request->nombres);
                $feligres->apellidos = Str::title($request->apellidos);
                $feligres->celular = $request->celular;
                $feligres->estado = 1;
                $feligres->save();
            }

            $visita_feli = new Visitafeligre();
            $visita_feli->visitacione_id = $visita_head_id;
            $visita_feli->feligre_id = $feligres->id;
            $visita_feli->comentario = $request->comentario;
            $visita_feli->save();

            $list_feligres =  DB::table('visitafeligres')->where('visitacione_id',$visita_head_id)
            ->leftJoin('feligres','feligres.id','=','visitafeligres.feligre_id')
            ->select('feligres.nombres','feligres.apellidos')
            ->get();

            DB::commit();

            return response()->json([
                "status" => 1,
                "msg" => "Creado con exito",
                "data" => $request->all(),
                "feligres"=>$feligres,
                "list_feligres"=>$list_feligres,
                // "persona" => $persona,
                // "distrito" => $distrito,
                "visita_id" => $visita_head_id
            ]);
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function misVisitas()
    {
        $persona = Persona::where('user_id', auth()->user()->id)->first();
        // dd($persona);

        // $misvisitas = Visitacione::with('feligres')
        // ->leftJoin('personas','personas.id','=','visitaciones.persona_id')
        // ->leftJoin('distritos','distritos.id','=','visitaciones.distrito_id')
        // ->leftJoin('tipovisitas','tipovisitas.id','=','visitaciones.tipovisita_id')
        // ->select('visitaciones.*','personas.apellidos','distritos.distrito','tipovisitas.tipo')
        // ->where('visitaciones.persona_id', $persona->id)
        // ->orderBy('visitaciones.id','DESC')
        // ->get();

        $porgrupos = DB::table('visitaciones')->where('visitaciones.persona_id',$persona->id)
        ->leftJoin('tipovisitas','tipovisitas.id','=','visitaciones.tipovisita_id')
        ->leftJoin('lugarvisitas','lugarvisitas.id','=','visitaciones.lugarvisita_id')
        ->select('lugarvisitas.lugar as lugarvisita','visitaciones.participantes','visitaciones.created_at','tipovisitas.tipo')
        ->get();

        $misvisitas = DB::table('visitafeligres')
        ->leftJoin('feligres','feligres.id','=','visitafeligres.feligre_id')
        ->leftJoin('visitaciones','visitaciones.id','=','visitafeligres.visitacione_id')
        ->leftJoin('tipovisitas','tipovisitas.id','=','visitaciones.tipovisita_id')
        ->select('feligres.nombres','feligres.apellidos','visitafeligres.created_at','tipovisitas.tipo')
        ->where('visitaciones.persona_id',$persona->id)
        ->get();

        return response()->json([
            "status" => 1,
            "msg" => "mis visitas",
            "data" => $misvisitas,
            'vistaxgrupos'=> $porgrupos
        ]);
    }

    public function getTipoVisita()
    {
        $listatipovisita = DB::table('tipovisitas')->where('estado', 1)->get();
        return response()->json([
            "status" => 1,
            "msg" => "tipo visitas",
            "data" => $listatipovisita
        ]);
    }
    public function getLugarVisita()
    {
        $listalugares = DB::table('lugarvisitas')->where('estado', 1)->get();
        return response()->json([
            "status" => 1,
            "msg" => "lugares de visitas",
            "data" => $listalugares
        ]);
    }

    public function consultaReniec($dni)
    {
        // clientevip1@tigpe.com
        $token = "c762f731632ee33ba1010f3b16b81b511e92e3dfe7e309c3205caa4271772860";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apiperu.dev/api/dni/" . $dni . "?api_token=" . $token,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            // echo "cURL Error #:" . $err;
            // $this->estadobusqueda = $err;
            return response()->json([
                "status" => 0,
                "msg" => "No existe data",
                "data" => $err
            ]);
        } else {
            $persona = json_decode($response);
            // $pers = (object)[
            //     'nombres' => $persona->data->nombres,
            //     'paterno' => $persona->data->apellido_paterno,
            //     'materno' => $persona->data->apellido_materno
            return response()->json([
                "status" => 1,
                // "success" => $persona,
                "msg" => 'hay data',
                "data" => $persona
            ]);
        }
    }
}
