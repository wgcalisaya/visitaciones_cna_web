<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(Request $request)
    {
        // if ($request->v_app != 'v3.16') {
        //     return response()->json([
        //         "status" => 0,
        //         "msg" => "Actualizar App Cliente VIP desde Play Store. Estamos en constante mejora. Presione aquí -> <a href='https://play.google.com/store/apps/details?id=pe.clientevip.app' target='../' > Actualizar ahora</a>"
        //     ], 404);
        // }

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('username', $request->email)->first();
        if (isset($user->id)) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken("auth_token")->plainTextToken;
                return response()->json([
                    "status" => 1,
                    "msg" => "Usuario logeado exitosamente",
                    "token" => $token,
                    "user"=> $user->persona,
                    "distrito"=>$user->persona->distrito
                ]);
            } else {
                return response()->json([
                    "status" => 0,
                    "msg" => "la contraseña es incorrecta"
                ], 404);
            }

            // if ($user->estado == 1) {
            // } else {
            //     return response()->json([
            //         "status" => 0,
            //         "msg" => "Usuario bloqueado por datos inválidos. Contactarse con el área de Soporte: 952633245 <a href='https://wa.me/51952633245?text=Hola, No puedo ingresar a la plataforma *CLIENTE VIP*' target='../' > Contactar</a>"
            //     ], 404);
            // }
        } else {
            return response()->json([
                "status" => 0,
                "msg" => "No existe usuario"
            ], 404);
            // return response()->json([
            //     "status" => 0,
            //     "msg" => "Si tienes inconvenientes para ingresar y te registraste entre el 16 y 19 de mayo, comunícate al 952633245 o escríbenos al whatsapp, <a href='https://wa.me/51952633245?text=Hola, No puedo ingresar a la plataforma *CLIENTE VIP*' target='../' > Click Aquí</a>"
            // ], 404);
        }
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        // Auth::user()->tokens()->delete();
        return response()->json([
            "status" => 1,
            "msg" => "Cesion cerrado"
        ]);
    }
}
