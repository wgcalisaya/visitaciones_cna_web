<?php

namespace App\Http\Controllers;

use App\Models\Distrito;
use App\Models\Persona;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('persona.distrito')->get();
        return view('admin.mantenimiento.personas.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $distritos = Distrito::where('estado', 1)->get();
        $roles = Role::get();
        return view('admin.mantenimiento.personas.crear', compact('distritos','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $user = User::where('id', $id)
        ->with('persona.distrito')
        ->first();
        // dd($user->getRoleNames());
        $userRole = $user->roles->all();
        // dd($userRole);

        $distritos = Distrito::where('estado', 1)->get();
        $roles = Role::get();
        return view('admin.mantenimiento.personas.editar', compact('user','distritos','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'nombres' => 'required|string|max:191',
            'apellidos' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'username' => 'required|string|max:191',
            // 'password' => 'required|string|min:6|confirmed',
            'celular' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $user = User::where('id',$id)->first();
            if ($request->has('nombres')) {
                $user->name = $request->nombres.' '.$request->apellidos;
            }
            if ($request->has('email')) {
                //$user->verification_token = User::generarVerificationToken();

                $validatedUserE = $request->validate([
                    'email' => 'required|string|max:191|unique:users,email,' .$user->id,
                ]);

                $user->email = $request->email;
            }

            if ($request->has('username') && $user->username != $request->username) {
                //$user->verification_token = User::generarVerificationToken();

                $validatedUserE = $request->validate([
                    'username' => 'required|string|max:191|unique:users,username,' .$user->id,
                ]);

                $user->username = $request->username;
            }

            if ($request->has('password') && $request->password != '') {
                $validatedUser = $request->validate([
                    'password'=>'min:6|confirmed',
                ]);

                $user->password = Hash::make($request->password);
            }
            $user->email = $request->email;
            $user->save();
            
            // $user = User::create([
            //     'name' => $request['nombres'].' '.$request['apellidos'],
            //     'email' => $request['email'],
            //     'password' => Hash::make($request['password'])
            // ]);
            $user->syncRoles($request->get('roles'));

            $persona = Persona::where('user_id', $user->id)->first();
            $persona->nombres = $request->nombres;
            $persona->apellidos = $request->apellidos;
            $persona->nrodoc = $request->nrodoc;
            $persona->estado = $request->estado;
            $persona->celular = $request->celular;
            $persona->email = $request->email;
            $persona->tipo = $request->tipo; //pastor - personal oficina(administrativo)
            $persona->save();

            if ($request->file('firma')) {
                $path = Storage::disk('public')->put('firma', $request->file('firma'));
                $persona->fill(['firma'=>$path])->save();
            }
            $distrito_act = Distrito::where('persona_id', $persona->id)->first();
            if ($distrito_act) {
                $distrito_act->persona_id = null;
                $distrito_act->save();
            }

            $distrito_new = Distrito::where('id', $request->distrito_id)->first();
            if ($distrito_new) {
                $distrito_new->persona_id = $persona->id;
                $distrito_new->save();
            }

            DB::commit();
            return redirect()->route('personal.index')
                ->with('info', 'El registro se guardo con éxito');           

        } catch (Exception $e) {
            DB::rollback();
            throw $e;

            return redirect()->route('personal.index')
                ->with('info', 'No se pudo hacer la operación.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
