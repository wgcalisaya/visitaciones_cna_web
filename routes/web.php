<?php

use App\Http\Controllers\PersonaController;
use App\Http\Controllers\Visitaciones\VisitacionesController;
use App\Http\Controllers\Visitaciones\VisitasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware(['role:Admin']);
    Route::get('/listardemo', [App\Http\Controllers\HomeController::class, 'demo'])->middleware(['role:Admin']);
    Route::get('/listarvisitas', [VisitacionesController::class, 'index'])->middleware(['role:Admin']);
    Route::get('admin/personal', [PersonaController::class, 'index'])->name('personal.index')->middleware(['role:Admin']);
    Route::get('admin/personal/create', [PersonaController::class, 'create'])->name('personal.create')->middleware(['role:Admin']);
    Route::get('admin/personal/{id}', [PersonaController::class, 'edit'])->middleware(['role:Admin']);
    Route::put('admin/personal/{id}', [PersonaController::class, 'update'])->name('personal.update')->middleware(['role:Admin']);
    Route::POST('admin/personal', [PersonaController::class, 'store'])->name('personal.store')->middleware(['role:Admin']);
});
