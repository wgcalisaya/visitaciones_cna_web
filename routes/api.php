<?php

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\VisitasController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [UserController::class, 'login']);
Route::group(['middleware' => ["auth:sanctum"]], function(){
    Route::post('registrarvisita', [VisitasController::class, 'crearVisita']);
    Route::get('gettipovisitas', [VisitasController::class, 'getTipoVisita']);
    Route::get('listarmisvisitas', [VisitasController::class, 'misVisitas']);
    Route::get('getlugares', [VisitasController::class, 'getLugarVisita']);
});

Route::get('get-dni-reniec/{dni}', [VisitasController::class, 'consultaReniec']);
